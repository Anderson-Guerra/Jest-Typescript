# **Template**

Runing template with [typescript](https://www.typescriptlang.org/) and [Jest](https://jestjs.io/).

## Installation 

```sh
    git clone https://gitlab.com/Anderson-Guerra/Jest-Typescript.git
    cd Jest-Typescript
    npm i
```

## Debug

Just click on debug tab **("Ctrl+Shift+D")**, select JEST and press play!<br />

![Jest](./.vscode/readme/jest.png "Run")

This action compile the typescript and existing tests.<br />
The result should appear on the terminal.<br />

![Jest](./.vscode/readme/jest-result.png "Result")

## License

[MIT license](https://opensource.org/licenses/MIT).
import { SendMessage } from './../dist/hello';

test('[line:03] should return "Hello World!"', () => {
  expect(SendMessage(true)).toMatch('Hello World!');
});

test('[line:07] should not return "Hello World!"', () => {
  expect(SendMessage(false)).not.toMatch('Hello World!');
});
